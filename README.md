# Safer Nanomaterials 2021

<img src="images/logos.PNG"  width="500">

----

## Practical: Pathway analysis - Molecular systems biology approaches for toxicity assessment

*  **Contact information:** Martina Summer-Kutmon (martina.kutmon (a) maastrichtuniversity.nl)
*  **Installation instructions:** [here](INSTALLATION.md)

Follow the installation instructions and make sure, you have downloaded **PathVisio**, the latest **WikiPathways human pathway collection** and the latest **BridgeDb human mapping** file.

----

**You can start the practical by following the instruction [here](PATHWAY.md).**

Look for other useful links onin the [Contact & Help page](CONTACT.md).
