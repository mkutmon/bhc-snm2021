# Pathway analysis with PathVisio

Pathway analysis helps you to interpret the data in a biological meaningful context. In this session, we will exploring the microarray gene expression data that studies the effects of silver nanoparticles on human intestine cell line Caco-2. The paper of the original experiment can be found here:

- Molecular mechanism of silver nanoparticles in human intestinal cells, Linda Böhmert, Birgit Niemann, Dajana Lichtenstein, Sabine Juling & Alfonso Lampen, Nanotoxicology, 2015; 9(7): 852–860, [Link](http://www.ncbi.nlm.nih.gov/pubmed/25997095).
- You can find more information about the dataset here: http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE62253 

In this small example dataset, we are comparing gene expression in Caco-2 cells after 24hrs exposure to silver nanoparticles with a control group. AS explained in the lecture, the log2 fold change indicates how much more or less a gene is expressed after exposure (positive = more expression, negative = less expression). The p-value gives us an indication about the significance of that change. 

Make sure you followed the [installation instructions](INSTALLATION.md) to install PathVisio before you start with the practical. 

## Step 0: Download required data

- Create a new folder "pathvisio-practical".
- In the [installation instructions](INSTALLATION.md), you should have already downloaded the Hs_Derby_Ensembl_91.brigde file and the human pathway collection from WikiPathways. Make sure they are both in the same folder and are unzipped (Right click > Extract). Make sure the .bridge file and the pathway collection folder are in the "pathvisio-practical" folder.
- Make sure the folder has no spaces or special characters (e.g. brackets).
- Downlod the [comp-silver-vs-control.txt](data/comp-silver-vs-control.txt) (right-click and Save Link As) and save it in the same folder.
- Download the [human Statin pathway](data/statin-pathway.gpml) file (right-click and Save Link As) and save it in the same folder. 

----

## Step 1: Get familiar with PathVisio

- Start PathVisio (pathvisio.bat on Windows or pathvisio.sh on Linux/MacOS)
- Open the human *Statin pathway* in PathVisio (File > Open > Browse to folder from step 0)

> **Question 1A:**
> Small numbers above data nodes, interactions or the info box in the top left of the pathway indicate publication references. Double click the info box in the top left (Title, Availability, Last modified, Organism) and go to the “Literature” tab.<br/>
> **What are the title and authors of the paper reference for this pathway?**

> **Question 1B:**
> Click on the DGAT1 gene in the top right. <br/>
> **With which identifier and database is this gene annotated?** (Check the “Backpage” tab on the right side)

- Load the identifier mapping database
  - Data > Select Gene Database > Browse to the folder from step 0 and load Hs_Derby_Ensembl_91.bridge
  - Check the status bar at the bottom to see if the gene database has been loaded

> **Question 1C:**
> Click on the DGAT1 gene in the top right again.<br/>
> **Can you now also find the Ensembl identifier(s) for this gene?** (Check the “Backpage” tab on the right side) - required for following steps!!

----

## Step 2: Import data in PathVisio
The statistically analysed dataset contains four columns: Ensembl, log2FC, FC and p-value. The first column contains the [Ensembl](http://www.ensembl.org/index.html) identifier (unique identifier for every gene). The fold change (FC) and log2 fold change (log2FC) show the differential expression between exposed cells and control. The p-value give the significance value. 

<img src="images/dataset.PNG"  width="350">

Now let's import the data in PathVisio:
- Data > Import Expression Data
- Select the comp-silver-vs-control.txt file as the input file. Everything else should be filled in automatically

<img src="images/import1.PNG"  width="350">

- In the next dialog, make sure the correct separators are used. You should see the four different columns in the preview

<img src="images/import2.PNG"  width="350">

- In the next step, you need to select the column that contains the identifier and the database. Select "Ensembl" as the primary identifier column and select "Use the same system code for all rows". Make sure you select **"Ensembl"** in that drop-down box (not Ensembl Human!). This tells PathVisio that all genes in the dataset are identified by Ensembl identifiers.

<img src="images/import3.PNG"  width="350">

- In the next step, you can see how many identifiers are recognized properly. It is normal if a **few** identifiers are not recognized (outdated identifiers, control sequences). 

> **Question 2A:**
> **How many rows were successfully imported?**

> **Question 2B:**
> **How many identifiers were not recognized?**
> If the number of rows is the same as the number of identifiers not recognized the data import was not done correctly - you probably didn’t select the correct database! Redo the import or ask one of the instructors for help!

If you click finish, you should see a default visualization on the pathway (if all genes are gray, the data import was not successful). If there is no pathway open, you can check the status bar at the bottom where the dataset will be listed.

----

## Step 3: Create data visualization

Follow the instruction to create a basic visualization for the log2FC:

- Data > Visualization Options
- Create a new visualization named "basic vis"

<img src="images/vis1.PNG"  width="350">

- Select "Text label"
- Select "Expression as color"
- In "Expression as color" select "Basic"
- Check the checkbox before "log2FC" and define a new color set 

<img src="images/vis2.PNG"  width="350">

- In the new dialog, select "Gradient" and define a gradient from -1 over 0 to 1 (blue - white - red). Then click OK. 

<img src="images/vis3.PNG"  width="350">

> **Question 3A:**
> Make a screenshot of the pathway. **What do the colors in the pathway mean biologically?** (Hint: Check the “Legend” tab on the right side). 

> **Question 3B:**
> Click on the DGAT1 gene in the top right. Go to the Data tab on the left side. **What is the log2FC of the DGAT1 gene?**

----

## Step 4: Perform pathway statistics

To identify pathways that might be affected after exposure, you can perform pathway statistics to calculate Z-Scores for each pathway (check lecture!). PathVisio automatically ranks the pathways based on the Z-Score. 

- Data > Statistics
- Define a criterion for the input genes. We will first look for significantly up-regulated pathways:
  `[log2FC] > 0.58 AND [p-value] < 0.05`

> **Question 4A:**
> **Explain in your own words what this expression criteria means (which genes will be selected)?**

- Next, we need to specify the pathway directory. Browse to the pathway collection folder downloaded in step 0. 
- Click on "Calculate" and wait for the result table. You can download the result table as a .csv file. Each row in the table is clickable and the pathway will be opened and the data will be visualized.

> **Question 4B:**
> **What are the top 10 up-regulated pathways and what are their Z-Scores? Do you see pathways that you would expect to be upregulated after nanoparticle exposure?**

Repeat the analysis for down-regulated pathways:
  `[log2FC] < -0.58 AND [p-value] < 0.05`

> **Question 4C**
> **What are the top 10 down-regulated pathways and what are their Z-Scores? Do you see pathways that you would expect to be downregulated after nanoparticle exposure?**
