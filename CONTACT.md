# Contact

* Dr. Martina Summer-Kutmon
* [Contact info](http://maastrichtuniversity.nl/martina.kutmon)

----

# Useful links

* [PathVisio homepage](https://pathvisio.github.io/)
* [Introduction pathway modelling](https://github.com/gladstone-institutes/Bioinformatics-Workshops/wiki/Introduction-to-Pathway-Modeling)
* [Pathway analysis in R](https://github.com/gladstone-institutes/Bioinformatics-Workshops/wiki/Introduction-to-Pathway-Analysis-Using-R)
* [WikiPathway academy - learn how to draw a pathway yourself](http://academy.wikipathways.org/)